## Libraries and tools ##


* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
* [DataBinding](https://developer.android.com/topic/libraries/data-binding)
* [Retrofit](https://square.github.io/retrofit/)
* [Navigation](https://developer.android.com/guide/navigation)
* [Room](https://developer.android.com/jetpack/androidx/releases/room)
* [Coroutines](https://developer.android.com/topic/libraries/architecture/coroutines)
* [Dagger Hilt](https://developer.android.com/training/dependency-injection/hilt-android)
* [Flow](https://developer.android.com/kotlin/flow)
* [Mockito](https://github.com/mockito/mockito)
* [Unit Test](https://developer.android.com/training/testing/local-tests)

* ## Architecture ##

* [MVVM Architecture](https://developer.android.com/jetpack/guide)
* [Multi Module](https://developer.android.com/topic/modularization)
