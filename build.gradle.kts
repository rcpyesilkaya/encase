// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    dependencies {
        classpath(com.example.builsSrc.ClassPaths.hiltAndroidGradlePlugin)
        classpath(com.example.builsSrc.ClassPaths.gradle)
        classpath(com.example.builsSrc.ClassPaths.kotlinGradle)
        classpath(com.example.builsSrc.ClassPaths.navigationSafeArgs)
    }
}

plugins {
    id(Plugins.androidApplication) version Versions.androidApplicationVersion apply false
    id(Plugins.androidLibrary) version Versions.androidLibraryVersion apply false
    id(Plugins.jetbrainsAndroidLibrary) version Versions.jetbrainsAndroidLibraryVersion apply false
    id(Plugins.jetBrainsKotlinJvm) version Versions.kotlinVersion apply false
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}