import com.example.builsSrc.Config

plugins {
    id(Plugins.androidApplication)
    id(Plugins.jetbrainsAndroidLibrary)
    id(Plugins.kotlinKapt)
    id(Plugins.daggerHiltAndroid)
    id(Plugins.kotlinParcelize)
    id(Plugins.navigationSafeArgs)
}

android {
    setCompileSdkVersion(Config.compileSdk)

    defaultConfig {
        applicationId = Config.applicationId
        minSdk = Config.minSdk
        targetSdk = Config.targetSdk
        versionCode = Config.versionCode
        versionName = Config.versionName

        testInstrumentationRunner = Config.testInstrumentationRunner
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(project(mapOf("path" to Modules.data)))
    implementation(project(mapOf("path" to Modules.domain)))
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.appcompat)
    implementation(Dependencies.material)
    implementation(Dependencies.constraintlayout)
    implementation(Dependencies.hiltAndroid)
    implementation(Dependencies.livedataKtx)
    implementation(Dependencies.viewmodelKtx)
    implementation(Dependencies.navigationFragment)
    implementation(Dependencies.navigationUi)
    implementation(Dependencies.glide)
    implementation(Dependencies.coroutinesAndroid)
    implementation(Dependencies.coroutinesCore)
    implementation(Dependencies.room)
    implementation(Dependencies.roomKtx)

    kapt(Dependencies.hiltCompiler)
    kapt(Dependencies.hiltAndroidCompiler)
    kapt(Dependencies.roomComplier)

    testImplementation(Dependencies.junit)
    testImplementation(Dependencies.coreTesting)
    testImplementation(Dependencies.kotlinxCoroutinesTest)
    testImplementation(Dependencies.truth)
    testImplementation(Dependencies.mockitoCore)
    testImplementation(Dependencies.mockitoInline)
    testImplementation(Dependencies.extJunit)
    testImplementation(Dependencies.robolectric)

    androidTestImplementation(Dependencies.junit)
    androidTestImplementation(Dependencies.mockitoAndroid)
    androidTestImplementation(Dependencies.kotlinxCoroutinesTest)
    androidTestImplementation(Dependencies.coreTesting)
    androidTestImplementation(Dependencies.truth)
    androidTestImplementation(Dependencies.mockitoCore)
    androidTestImplementation(Dependencies.mockitoInline)
    androidTestImplementation(Dependencies.extJunit)
    androidTestImplementation(Dependencies.espressoCore)
}