package com.example.encase.ui.home

import com.example.domain.core.Resource
import com.example.domain.remote.EnuygunUseCase
import com.example.encase.ui.home.util.HomeViewState
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@OptIn(ExperimentalCoroutinesApi::class)
class HomeViewModelTest {

    @Mock
    private lateinit var enuygunUseCase: EnuygunUseCase

    @get:Rule
    private val testCoroutineRule = TestCoroutineRule()
    private lateinit var viewModel: HomeViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineRule.dispatcher)
        MockitoAnnotations.openMocks(this)
        viewModel = HomeViewModel(enuygunUseCase)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `filterList should set FilterErrorEmptyPrice when minPrice or maxPrice is empty`() = runTest {
        viewModel.filterList("", "5000")
        assertEquals(HomeViewState.FilterErrorEmptyPrice, viewModel.homeFlow.value)
    }

    @Test
    fun `filterList should set FilterErrorPriceMinMax when minPrice is greater than maxPrice`() = runTest {
        viewModel.filterList("5000", "3000")
        assertEquals(HomeViewState.FilterErrorPriceMinMax, viewModel.homeFlow.value)
    }

    @Test
    fun `filterList should set FilterSuccess when minPrice and maxPrice are valid`() = runTest {
        viewModel.enuygunList = mockList()
        viewModel.filterList("1000", "3000")
        assertEquals(expectedResult(), (viewModel.homeFlow.value as HomeViewState.FilterSuccess).data)
    }

    @Test
    fun `fetchList should set FetchList state when use case returns data`() = runTest {
        val mockEnuygunList = mockList()
        Mockito.`when`(enuygunUseCase.invoke(Unit)).thenReturn(flow { emit(Resource.Success(mockEnuygunList)) })
        viewModel.fetchList()
        assertTrue(viewModel.homeFlow.value is HomeViewState.FetchList)
    }

    @Test
    fun `fetchList should set Loading state when use case is loading`() = runTest {
        Mockito.`when`(enuygunUseCase.invoke(Unit)).thenReturn(flow { emit(Resource.Loading) })
        viewModel.fetchList()
        assertEquals(HomeViewState.Loading, viewModel.homeFlow.value)
    }

    @Test
    fun `fetchList should set Error state when use case returns an error`() = runTest {
        val mockError = Resource.Error(400, "error")
        Mockito.`when`(enuygunUseCase.invoke(Unit)).thenReturn(flow { emit(mockError) })
        viewModel.fetchList()
        assertTrue(viewModel.homeFlow.value is HomeViewState.Error)
    }

}