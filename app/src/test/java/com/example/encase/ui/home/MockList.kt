package com.example.encase.ui.home

import com.example.domain.remote.Enuygun

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

fun mockList(): ArrayList<Enuygun> {
    val enuygunList = ArrayList<Enuygun>()

    repeat(10) {
        enuygunList.add(
            Enuygun(
                id = it + 1,
                name = "Mackbook Pro ${it + 1}",
                priceStr = " ${(it + 1) * 1000}TL",
                price = (it + 1) * 1000,
                desc = "MackBook Pro 2021",
                isFavorite = false
            )
        )
    }
    return enuygunList
}

fun expectedResult(): ArrayList<Enuygun> {
    val enuygunList = ArrayList<Enuygun>()
    for (it in 1..3) {

        enuygunList.add(
            Enuygun(
                id = it,
                name = "Mackbook Pro $it",
                priceStr = " ${it * 1000}TL",
                price = it * 1000,
                desc = "MackBook Pro 2021",
                isFavorite = false
            )
        )
    }
    return enuygunList
}