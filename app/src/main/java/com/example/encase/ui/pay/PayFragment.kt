package com.example.encase.ui.pay

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.encase.R
import com.example.encase.databinding.FragmentPayBinding
import com.example.encase.ui.pay.util.PayViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

@AndroidEntryPoint
class PayFragment : Fragment() {

    private lateinit var binding: FragmentPayBinding
    private val viewModel by viewModels<PayViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPayBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFlow()
        initOnClickListener()
    }

    private fun initFlow() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.payFlow.collect {
                when (it) {
                    is PayViewState.InputsEmpty -> {
                        Toast.makeText(requireContext(), getString(R.string.fill_in_all_fields), Toast.LENGTH_SHORT).show()
                    }
                    is PayViewState.DeleteBasket -> {
                        binding.editTextName.setText("")
                        binding.editTextMail.setText("")
                        binding.editTextPhone.setText("")
                        Toast.makeText(requireContext(), getString(R.string.payment_success), Toast.LENGTH_SHORT).show()
                        activity?.onBackPressed()
                    }
                    is PayViewState.Loading -> {}
                    is PayViewState.Error -> {}
                    is PayViewState.Idle -> {}
                    else -> {}
                }
            }
        }
    }

    private fun initOnClickListener() {
        binding.buttonPay.setOnClickListener {
            val name = binding.editTextName.text
            val mail = binding.editTextMail.text
            val phone = binding.editTextPhone.text
            viewModel.validateInputs(name, mail, phone)
        }
    }
}