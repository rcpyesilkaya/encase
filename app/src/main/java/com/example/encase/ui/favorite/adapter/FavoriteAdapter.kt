package com.example.encase.ui.favorite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.remote.Enuygun
import com.example.encase.databinding.ItemFavoriteBinding
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class FavoriteAdapter @Inject constructor() : RecyclerView.Adapter<FavoriteItemViewHolder>() {

    var onClickListener: ((Enuygun) -> Unit)? = null
    var onClickFavoriteIconListener: ((Enuygun) -> Unit)? = null
    private var favoriteList = listOf<Enuygun>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemFavoriteBinding.inflate(inflater, parent, false)
        return FavoriteItemViewHolder(binding = binding)
    }

    override fun getItemCount() = favoriteList.size

    override fun onBindViewHolder(holder: FavoriteItemViewHolder, position: Int) {
        val data = favoriteList[position]
        holder.onBind(data, onClickListener,onClickFavoriteIconListener)
    }

    fun setFavoriteList(favoriteList: ArrayList<Enuygun>) {
        this.favoriteList = favoriteList
    }
}