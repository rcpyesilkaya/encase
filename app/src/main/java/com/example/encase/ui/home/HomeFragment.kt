package com.example.encase.ui.home

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.domain.remote.Enuygun
import com.example.encase.R
import com.example.encase.databinding.FragmentHomeBinding
import com.example.encase.databinding.LayoutFilterDialogBinding
import com.example.encase.databinding.LayoutSortDialogBinding
import com.example.encase.ui.home.adapter.HomeAdapter
import com.example.encase.ui.home.util.HomeViewData
import com.example.encase.ui.home.util.HomeViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel by viewModels<HomeViewModel>()
    var searchList = arrayListOf<Enuygun>()
    private lateinit var mBuilder: AlertDialog

    @Inject
    lateinit var homeAdapter: HomeAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFlow()
        viewModel.fetchList()
        initAdapter()
        initSearchView()
        initOnClickListener()
    }

    private fun initFlow() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.homeFlow.collect {
                binding.viewData = HomeViewData(it)
                when (it) {
                    is HomeViewState.FetchList -> {
                        homeAdapter.setEnuygunList(it.data)
                        searchList = it.data
                    }
                    is HomeViewState.Loading -> {}
                    is HomeViewState.Error -> {}
                    is HomeViewState.Idle -> {}
                    is HomeViewState.FilterErrorEmptyPrice -> {
                        Toast.makeText(requireContext(), getString(R.string.please_enter_price), Toast.LENGTH_LONG).show()
                    }
                    is HomeViewState.FilterErrorPriceMinMax -> {
                        Toast.makeText(requireContext(), getString(R.string.min_price_error), Toast.LENGTH_LONG).show()
                    }
                    is HomeViewState.FilterSuccess -> {
                        homeAdapter.setEnuygunList(it.data)
                        homeAdapter.notifyDataSetChanged()
                        mBuilder.cancel()
                    }
                    is HomeViewState.SortedList -> {
                        homeAdapter.setEnuygunList(ArrayList(it.data))
                        homeAdapter.notifyDataSetChanged()
                        mBuilder.cancel()
                    }
                }
            }
        }
    }

    private fun initAdapter() {
        homeAdapter.onClickListener = {
            val action = HomeFragmentDirections.actionNavigationHomeToDetailFragment(it)
            findNavController().navigate(action)
        }
        binding.recyclerView.adapter = homeAdapter
    }

    private fun initOnClickListener() {
        binding.buttonSorting.setOnClickListener {
            bindAlertSort()
        }
        binding.buttonFilter.setOnClickListener {
            bindAlertFilter()
        }
    }

    private fun initSearchView() {
        binding.searchView.setOnTouchListener { _, _ ->
            binding.searchView.isIconified = false
            binding.searchView.requestFocus()
            false
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                binding.searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchList = arrayListOf()
                val searchText = newText?.lowercase(Locale.getDefault())
                if (searchText.isNullOrEmpty().not()) {
                    viewModel.enuygunList.filter { it.name.lowercase(Locale.getDefault()).contains(searchText!!) }.let {
                        searchList.addAll(it)
                    }
                    homeAdapter.setEnuygunList(searchList)
                    homeAdapter.notifyDataSetChanged()
                } else {
                    searchList = arrayListOf()
                    searchList.addAll(viewModel.enuygunList)
                    homeAdapter.setEnuygunList(searchList)
                    homeAdapter.notifyDataSetChanged()
                }
                return false
            }
        })
    }

    private fun bindAlertSort() {
        val layoutSortBinding = LayoutSortDialogBinding.inflate(
            LayoutInflater.from(context),
            binding.root as ViewGroup,
            false
        )
        mBuilder = AlertDialog.Builder(context).setView(layoutSortBinding.root).show()
        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        layoutSortBinding.buttonMaxPrice.setOnClickListener {
            viewModel.sortList(false)
        }
        layoutSortBinding.buttonMinPrice.setOnClickListener {
            viewModel.sortList(true)
        }
    }

    private fun bindAlertFilter() {
        val layoutFilterDialogBinding = LayoutFilterDialogBinding.inflate(
            LayoutInflater.from(context),
            binding.root as ViewGroup,
            false
        )
        mBuilder = AlertDialog.Builder(context).setView(layoutFilterDialogBinding.root).show()
        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        layoutFilterDialogBinding.buttonFilter.setOnClickListener {
            val minPrice = layoutFilterDialogBinding.editTextLowestPrice.text.toString()
            val maxPrice = layoutFilterDialogBinding.editTextHighestPrice.text.toString()
            viewModel.filterList(minPrice, maxPrice)
        }
    }
}