package com.example.encase.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.remote.Enuygun
import com.example.encase.databinding.ItemListBinding
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

class HomeAdapter @Inject constructor() : RecyclerView.Adapter<ItemViewHolder>() {

    var onClickListener: ((Enuygun) -> Unit)? = null
    private var enuygunList = listOf<Enuygun>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListBinding.inflate(inflater, parent, false)
        return ItemViewHolder(binding = binding)
    }

    override fun getItemCount() = enuygunList.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val data = enuygunList[position]
        holder.onBind(data,onClickListener)
    }

    fun setEnuygunList(enuygunList: ArrayList<Enuygun>) {
        this.enuygunList = enuygunList
    }
}