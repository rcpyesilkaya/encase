package com.example.encase.ui.pay

import android.text.Editable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.core.onError
import com.example.domain.core.onLoading
import com.example.domain.core.onSuccess
import com.example.domain.local.Cart
import com.example.domain.local.cartitem.DeleteCartItemUseCase
import com.example.domain.local.cartitem.GetAllCartItemsUseCase
import com.example.encase.ui.pay.util.PayViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 11.03.2024.
 */

@HiltViewModel
class PayViewModel @Inject constructor(
    private val getAllCartItemsUseCase: GetAllCartItemsUseCase,
    private val deleteCartItemUseCase: DeleteCartItemUseCase,
) : ViewModel() {

    private val _payFlow = MutableSharedFlow<PayViewState>()
    val payFlow: SharedFlow<PayViewState> = _payFlow

    private fun deleteBasket() {
        viewModelScope.launch {
            getAllCartItemsUseCase(Unit).collect {
                it.onSuccess {
                    it?.forEach {
                        deleteCartItem(Cart(it.enuygun.id, 0))
                    }
                    delay(1500)
                    _payFlow.emit(PayViewState.DeleteBasket)
                }.onLoading {
                    _payFlow.emit(PayViewState.Loading)
                }.onError {
                    _payFlow.emit(PayViewState.Error(it))
                }
            }
        }
    }

    private fun deleteCartItem(cart: Cart) {
        viewModelScope.launch {
            deleteCartItemUseCase(cart).collect {
                it.onSuccess {}.onLoading {
                    _payFlow.emit(PayViewState.Loading)
                }.onError {
                    _payFlow.emit(PayViewState.Error(it))
                }
            }
        }
    }

    fun validateInputs(name: Editable?, mail: Editable?, phone: Editable?) {
        viewModelScope.launch {
            if (name.isNullOrEmpty() || mail.isNullOrEmpty() || phone.isNullOrEmpty()) {
                _payFlow.emit(PayViewState.InputsEmpty)
            } else {
                deleteBasket()
            }
        }
    }
}