package com.example.encase.ui.basket.adapter

import androidx.recyclerview.widget.RecyclerView
import com.example.encase.databinding.ItemBasketBinding
import com.example.domain.local.cartitem.Basket

class BasketItemViewHolder(private val binding: ItemBasketBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun onBind(
        data: Basket,
        onDeleteClickListener: ((Basket) -> Unit)?,
        onMinusClickListener: ((Basket) -> Unit)?,
        onPlusClickListener: ((Basket) -> Unit)?
    ) {
        binding.viewData = BasketItemViewData(data)

        binding.imageViewDelete.setOnClickListener {
            onDeleteClickListener?.invoke(data)
        }
        binding.imageViewMinus.setOnClickListener {
            onMinusClickListener?.invoke(data)
        }
        binding.imageViewPlus.setOnClickListener {
            onPlusClickListener?.invoke(data)
        }
    }
}
