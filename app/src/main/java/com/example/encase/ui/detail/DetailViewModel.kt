package com.example.encase.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.core.onError
import com.example.domain.core.onLoading
import com.example.domain.core.onSuccess
import com.example.domain.local.Cart
import com.example.domain.local.cartitem.InsertCartItemUseCase
import com.example.domain.remote.Enuygun
import com.example.domain.local.favorite.AddFavoriteUseCase
import com.example.domain.local.favorite.RemoveFavoriteUseCase
import com.example.domain.local.favorite.GetFavoriteItemByIdUseCase
import com.example.encase.ui.detail.util.DetailViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val addFavoriteUseCase: AddFavoriteUseCase,
    private val removeFavoriteUseCase: RemoveFavoriteUseCase,
    private val getFavoriteItemByIdUseCase: GetFavoriteItemByIdUseCase,
    private val insertCartItemUseCase: InsertCartItemUseCase,
) : ViewModel() {

    private val _detailFlow = MutableStateFlow<DetailViewState>(DetailViewState.Idle)
    val detailFlow: StateFlow<DetailViewState> = _detailFlow

    fun addFavorite(item: Enuygun) {
        viewModelScope.launch {
            addFavoriteUseCase(item).collect {
                it.onSuccess {
                    _detailFlow.value = DetailViewState.AddFavorite
                }.onLoading {
                    _detailFlow.value = DetailViewState.Loading
                }.onError {
                    _detailFlow.value = DetailViewState.Error(it)
                }
            }
        }
    }

    fun removeFavorite(item: Enuygun) {
        viewModelScope.launch {
            removeFavoriteUseCase(item).collect {
                it.onSuccess {
                    _detailFlow.value = DetailViewState.RemoveFavorite
                }.onLoading {
                    _detailFlow.value = DetailViewState.Loading
                }.onError {
                    _detailFlow.value = DetailViewState.Error(it)
                }
            }
        }
    }

    fun getFavoriteItemById(id: Int) {
        viewModelScope.launch {
            getFavoriteItemByIdUseCase(id).collect {
                it.onSuccess {
                    _detailFlow.value = DetailViewState.GetFavoriteItemById(it)
                }.onLoading {
                    _detailFlow.value = DetailViewState.Loading
                }.onError {
                    _detailFlow.value = DetailViewState.Error(it)
                }
            }
        }
    }

    fun insertCartItem(cart: Cart) {
        viewModelScope.launch {
            insertCartItemUseCase(cart).collect {
                it.onSuccess {
                    _detailFlow.value = DetailViewState.InsertCartItem
                }.onLoading {
                    _detailFlow.value = DetailViewState.Loading
                }.onError {
                    _detailFlow.value = DetailViewState.Error(it)
                }
            }
        }
    }
}