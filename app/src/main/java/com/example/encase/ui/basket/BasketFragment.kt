package com.example.encase.ui.basket

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.domain.local.Cart
import com.example.encase.databinding.FragmentBasketBinding
import com.example.encase.ui.basket.adapter.BasketAdapter
import com.example.encase.ui.basket.util.BasketViewData
import com.example.encase.ui.basket.util.BasketViewState
import com.example.encase.ui.favorite.FavoriteFragmentDirections
import com.example.encase.ui.favorite.util.FavoriteViewData
import com.example.encase.ui.favorite.util.FavoriteViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class BasketFragment : Fragment() {

    private lateinit var binding: FragmentBasketBinding
    private val viewModel by viewModels<BasketViewModel>()

    @Inject
    lateinit var basketAdapter: BasketAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBasketBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initFlow()
        viewModel.getAllCartItems()
        initOnClickListener()
    }

    private fun initFlow() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.basketFlow.collect {
                binding.viewData = BasketViewData(it)
                when (it) {
                    is BasketViewState.GetAllCartItems -> {
                        basketAdapter.setBasketList(it.data)
                        basketAdapter.notifyDataSetChanged()
                    }
                    is BasketViewState.Loading -> {}
                    is BasketViewState.Error -> {}
                    is BasketViewState.Idle -> {}
                    else -> {}
                }
            }
        }
    }

    private fun initAdapter() {
        basketAdapter.onDeleteClickListener = {
            viewModel.deleteCartItem(Cart(enuygunId = it.enuygun.id, quantity = 0))
        }
        basketAdapter.onMinusClickListener = {
            viewModel.deleteCartItem(Cart(enuygunId = it.enuygun.id, quantity = 1))

        }
        basketAdapter.onPlusClickListener = {
            viewModel.insertCartItem(Cart(enuygunId = it.enuygun.id, quantity = 1))
        }
        binding.recyclerViewBasket.adapter = basketAdapter
    }

    private fun initOnClickListener() {
        binding.buttonCheckout.setOnClickListener {
            val action = BasketFragmentDirections.actionNavigationBasketToPayFragment()
            findNavController().navigate(action)
        }
    }

}