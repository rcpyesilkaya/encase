package com.example.encase.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.encase.databinding.FragmentFavoriteBinding
import com.example.encase.ui.favorite.adapter.FavoriteAdapter
import com.example.encase.ui.favorite.util.FavoriteViewData
import com.example.encase.ui.favorite.util.FavoriteViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class FavoriteFragment : Fragment() {

    private lateinit var binding: FragmentFavoriteBinding
    private val viewModel by viewModels<FavoriteViewModel>()

    @Inject
    lateinit var favoriteAdapter: FavoriteAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavoriteBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initFlow()
        viewModel.getAllFavorites()
    }

    private fun initAdapter() {
        favoriteAdapter.onClickListener = {
            val action = FavoriteFragmentDirections.actionNavigationFavoriteToDetailFragment(it)
            findNavController().navigate(action)
        }
        favoriteAdapter.onClickFavoriteIconListener = {
            viewModel.removeFavorite(it)
        }
        binding.recyclerViewFavorite.adapter = favoriteAdapter
    }

    private fun initFlow() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.favoriteFlow.collect {
                binding.viewData = FavoriteViewData(it)
                when (it) {
                    is FavoriteViewState.GetAllFavorites -> {
                        favoriteAdapter.setFavoriteList(it.data)
                        favoriteAdapter.notifyDataSetChanged()
                    }
                    is FavoriteViewState.Loading -> {}
                    is FavoriteViewState.Error -> {}
                    is FavoriteViewState.Idle -> {}
                    else -> {}
                }
            }
        }
    }
}