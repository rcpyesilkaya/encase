package com.example.encase.ui.favorite.util

import android.view.View

data class FavoriteViewData(val state: FavoriteViewState) {
    fun progressVisibility() = if (state is FavoriteViewState.Loading) View.VISIBLE else View.GONE
    fun recyclerViewVisibility() = if (state is FavoriteViewState.GetAllFavorites) View.VISIBLE else View.GONE
    fun errorVisibility() = if (state is FavoriteViewState.Error) View.VISIBLE else View.GONE
    fun errorText() = if (state is FavoriteViewState.Error) state.error.message else "Favori listenize ulaşılamadı"
}