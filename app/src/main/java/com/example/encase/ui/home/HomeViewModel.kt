package com.example.encase.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.core.onError
import com.example.domain.core.onLoading
import com.example.domain.core.onSuccess
import com.example.domain.remote.Enuygun
import com.example.domain.remote.EnuygunUseCase
import com.example.encase.ui.home.util.HomeViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val enuygunUseCase: EnuygunUseCase
) : ViewModel() {

    private val _homeFlow = MutableStateFlow<HomeViewState>(HomeViewState.Idle)
    val homeFlow: StateFlow<HomeViewState> = _homeFlow
    var enuygunList = listOf<Enuygun>()

    fun fetchList() {
        viewModelScope.launch {
            enuygunUseCase(Unit).collect {
                it.onSuccess {
                    enuygunList = it
                    _homeFlow.value = HomeViewState.FetchList(it as ArrayList<Enuygun>)
                }.onLoading {
                    _homeFlow.value = HomeViewState.Loading
                }.onError {
                    _homeFlow.value = HomeViewState.Error(it)
                }
            }
        }
    }

    fun sortList(isSortedBy: Boolean) {
       val sortedList = if (isSortedBy) {
            enuygunList.sortedBy { it.price }
        } else {
            enuygunList.sortedByDescending { it.price }
        }
        _homeFlow.value = HomeViewState.SortedList(sortedList)
    }

    fun filterList(minPrice: String, maxPrice: String) {
        if (minPrice.isEmpty() || maxPrice.isEmpty()) {
            _homeFlow.value = HomeViewState.FilterErrorEmptyPrice
        } else if (minPrice.toInt() > maxPrice.toInt()) {
            _homeFlow.value = HomeViewState.FilterErrorPriceMinMax
        } else {
            val filteredList = enuygunList.filter { it.price >= minPrice.toInt() && it.price <= maxPrice.toInt() }
            _homeFlow.value = HomeViewState.FilterSuccess(java.util.ArrayList(filteredList))
        }
    }
}