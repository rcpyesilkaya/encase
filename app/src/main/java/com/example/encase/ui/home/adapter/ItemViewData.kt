package com.example.encase.ui.home.adapter

import com.example.domain.remote.Enuygun

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

data class ItemViewData(
    val data : Enuygun
){
    fun name() = data.name
    fun desc() = data.desc
    fun priceStr() = data.priceStr
}
