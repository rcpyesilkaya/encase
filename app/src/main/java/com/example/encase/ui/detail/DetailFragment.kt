package com.example.encase.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.domain.local.Cart
import com.example.encase.R
import com.example.encase.databinding.FragmentDetailBinding
import com.example.encase.ui.detail.util.DetailViewData
import com.example.encase.ui.detail.util.DetailViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@AndroidEntryPoint
class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    private val detailArgs by navArgs<DetailFragmentArgs>()
    private val viewModel by viewModels<DetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewData = DetailViewData(detailArgs.enuygun)
        initOnClickListener()
        initFlow()
        initUi()
    }

    private fun initOnClickListener() {
        binding.imageViewFavorite.setOnClickListener {
            if (detailArgs.enuygun.isFavorite) {
                viewModel.removeFavorite(detailArgs.enuygun)
            } else {
                viewModel.addFavorite(detailArgs.enuygun)
            }
        }

        binding.buttonAddToCart.setOnClickListener {
            viewModel.insertCartItem(Cart(detailArgs.enuygun.id, 1))
        }
    }

    private fun initFlow() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.detailFlow.collect {
                when (it) {
                    is DetailViewState.AddFavorite, DetailViewState.RemoveFavorite -> {
                        detailArgs.enuygun.isFavorite = detailArgs.enuygun.isFavorite.not()
                        binding.viewData = DetailViewData(detailArgs.enuygun)
                    }
                    is DetailViewState.Loading -> {}
                    is DetailViewState.Error -> {}
                    is DetailViewState.Idle -> {}
                    is DetailViewState.InsertCartItem -> {
                        Toast.makeText(requireContext(), getString(R.string.add_cart), Toast.LENGTH_SHORT).show()
                    }
                    is DetailViewState.GetFavoriteItemById -> {
                        detailArgs.enuygun.isFavorite = it.data.name.isEmpty().not()
                        binding.viewData = DetailViewData(detailArgs.enuygun)
                    }
                }
            }
        }
    }

    private fun initUi() {
        viewModel.getFavoriteItemById(detailArgs.enuygun.id)
    }
}