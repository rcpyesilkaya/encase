package com.example.encase.ui.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.core.Resource
import com.example.domain.core.onError
import com.example.domain.core.onLoading
import com.example.domain.core.onSuccess
import com.example.domain.remote.Enuygun
import com.example.domain.local.favorite.GetAllFavoritesUseCase
import com.example.domain.local.favorite.RemoveFavoriteUseCase
import com.example.encase.ui.favorite.util.FavoriteViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val getAllFavoritesUseCase: GetAllFavoritesUseCase,
    private val removeFavoriteUseCase: RemoveFavoriteUseCase,
) : ViewModel() {

    private val _favoriteFlow = MutableStateFlow<FavoriteViewState>(FavoriteViewState.Idle)
    val favoriteFlow: StateFlow<FavoriteViewState> = _favoriteFlow

    fun getAllFavorites() {
        viewModelScope.launch {
            getAllFavoritesUseCase(Unit).collect {
                it.onSuccess {
                    if (it.isNullOrEmpty()) {
                        _favoriteFlow.value = FavoriteViewState.Error(Resource.Error(code = 400, "Favori Listeniz Boş"))
                    } else {
                        _favoriteFlow.value = FavoriteViewState.GetAllFavorites(ArrayList(it))
                    }
                }.onLoading {
                    _favoriteFlow.value = FavoriteViewState.Loading
                }.onError {
                    _favoriteFlow.value = FavoriteViewState.Error(it)
                }
            }
        }
    }

    fun removeFavorite(item: Enuygun) {
        viewModelScope.launch {
            removeFavoriteUseCase(item).collect {
                it.onSuccess {
                    getAllFavorites()
                }.onLoading {
                    _favoriteFlow.value = FavoriteViewState.Loading
                }.onError {
                    _favoriteFlow.value = FavoriteViewState.Error(it)
                }
            }
        }
    }
}