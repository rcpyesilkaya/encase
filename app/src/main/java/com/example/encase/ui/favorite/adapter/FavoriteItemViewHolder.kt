package com.example.encase.ui.favorite.adapter

import androidx.recyclerview.widget.RecyclerView
import com.example.domain.remote.Enuygun
import com.example.encase.databinding.ItemFavoriteBinding
import com.example.encase.ui.home.adapter.ItemViewData

class FavoriteItemViewHolder(private val binding: ItemFavoriteBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun onBind(data: Enuygun, onClickListener: ((Enuygun) -> Unit)?, onClickFavoriteIconListener: ((Enuygun) -> Unit)?) {
        binding.viewData = ItemViewData(data)

        binding.imageViewFavorite.setOnClickListener {
            onClickFavoriteIconListener?.invoke(data)
        }

        binding.root.setOnClickListener {
            onClickListener?.invoke(data)
        }
    }
}
