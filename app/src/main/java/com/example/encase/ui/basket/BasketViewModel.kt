package com.example.encase.ui.basket

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.core.Resource
import com.example.domain.core.onError
import com.example.domain.core.onLoading
import com.example.domain.core.onSuccess
import com.example.domain.local.Cart
import com.example.domain.local.cartitem.DeleteCartItemUseCase
import com.example.domain.local.cartitem.GetAllCartItemsUseCase
import com.example.domain.local.cartitem.InsertCartItemUseCase
import com.example.encase.ui.basket.util.BasketViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BasketViewModel @Inject constructor(
    private val deleteCartItemUseCase: DeleteCartItemUseCase,
    private val insertCartItemUseCase: InsertCartItemUseCase,
    private val getAllCartItemsUseCase: GetAllCartItemsUseCase,
) : ViewModel() {

    private val _basketFlow = MutableStateFlow<BasketViewState>(BasketViewState.Idle)
    val basketFlow: StateFlow<BasketViewState> = _basketFlow

    fun deleteCartItem(cart: Cart) {
        viewModelScope.launch {
            deleteCartItemUseCase(cart).collect {
                it.onSuccess {
                    getAllCartItems()
                }.onLoading {
                    _basketFlow.value = BasketViewState.Loading
                }.onError {
                    _basketFlow.value = BasketViewState.Error(it)
                }
            }
        }
    }

    fun insertCartItem(cart: Cart) {
        viewModelScope.launch {
            insertCartItemUseCase(cart).collect {
                it.onSuccess {
                    getAllCartItems()
                }.onLoading {
                    _basketFlow.value = BasketViewState.Loading
                }.onError {
                    _basketFlow.value = BasketViewState.Error(it)
                }
            }
        }
    }

    fun getAllCartItems() {
        viewModelScope.launch {
            getAllCartItemsUseCase(Unit).collect {
                it.onSuccess {
                    if (it.isNullOrEmpty()) {
                        _basketFlow.value = BasketViewState.Error(Resource.Error(code = 400, "Sepetiniz Boş"))
                    } else {
                        _basketFlow.value = BasketViewState.GetAllCartItems(ArrayList(it))
                    }
                }.onLoading {
                    _basketFlow.value = BasketViewState.Loading
                }.onError {
                    _basketFlow.value = BasketViewState.Error(it)
                }
            }
        }
    }
}