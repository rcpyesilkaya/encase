package com.example.encase.ui.basket.util

import com.example.domain.core.Resource
import com.example.domain.local.cartitem.Basket

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

sealed class BasketViewState {
    object Idle : BasketViewState()
    object Loading : BasketViewState()
    class GetAllCartItems(val data: ArrayList<Basket>) : BasketViewState()
    class Error(val error: Resource.Error) : BasketViewState()
}
