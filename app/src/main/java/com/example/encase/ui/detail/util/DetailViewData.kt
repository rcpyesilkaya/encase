package com.example.encase.ui.detail.util

import android.content.Context
import com.example.domain.remote.Enuygun
import com.example.encase.R

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

data class DetailViewData(private val enuygun: Enuygun) {
    fun name() = enuygun.name
    fun desc() = enuygun.desc
    fun priceStr() = enuygun.priceStr
    fun isFavorie(context: Context) =
        if (enuygun.isFavorite) context.getDrawable(R.drawable.star) else context.getDrawable(R.drawable.star_outline)
}
