package com.example.encase.ui.home.adapter

import androidx.recyclerview.widget.RecyclerView
import com.example.domain.remote.Enuygun
import com.example.encase.databinding.ItemListBinding

class ItemViewHolder(private val binding: ItemListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun onBind(data: Enuygun, onClickListener: ((Enuygun) -> Unit)?) {
        binding.viewData = ItemViewData(data)
        binding.root.setOnClickListener {
            onClickListener?.invoke(data)
        }
    }
}
