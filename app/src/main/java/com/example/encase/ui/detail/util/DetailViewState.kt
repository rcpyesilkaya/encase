package com.example.encase.ui.detail.util

import com.example.domain.core.Resource
import com.example.domain.remote.Enuygun

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

sealed class DetailViewState {
    object Idle : DetailViewState()
    object Loading : DetailViewState()
    class Error(val error: Resource.Error) : DetailViewState()
    object AddFavorite : DetailViewState()
    object RemoveFavorite : DetailViewState()
    class GetFavoriteItemById(val data : Enuygun) : DetailViewState()
    object InsertCartItem : DetailViewState()
}
