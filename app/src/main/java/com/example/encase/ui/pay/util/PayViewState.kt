package com.example.encase.ui.pay.util

import com.example.domain.core.Resource
import com.example.domain.local.cartitem.Basket

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

sealed class PayViewState {
    object Idle : PayViewState()
    object Loading : PayViewState()
    object InputsEmpty : PayViewState()
    class PaymentSuccess(val data: ArrayList<Basket>) : PayViewState()
    class Error(val error: Resource.Error) : PayViewState()
    object DeleteBasket : PayViewState()
}
