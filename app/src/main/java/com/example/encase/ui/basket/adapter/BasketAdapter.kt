package com.example.encase.ui.basket.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.encase.databinding.ItemBasketBinding
import com.example.domain.local.cartitem.Basket
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class BasketAdapter @Inject constructor() : RecyclerView.Adapter<BasketItemViewHolder>() {

    var onDeleteClickListener: ((Basket) -> Unit)? = null
    var onMinusClickListener: ((Basket) -> Unit)? = null
    var onPlusClickListener: ((Basket) -> Unit)? = null
    private var basketList = listOf<Basket>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasketItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemBasketBinding.inflate(inflater, parent, false)
        return BasketItemViewHolder(binding = binding)
    }

    override fun getItemCount() = basketList.size

    override fun onBindViewHolder(holder: BasketItemViewHolder, position: Int) {
        val data = basketList[position]
        holder.onBind(data, onDeleteClickListener, onMinusClickListener, onPlusClickListener)
    }

    fun setBasketList(basketList: ArrayList<Basket>) {
        this.basketList = basketList
    }
}