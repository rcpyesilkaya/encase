package com.example.encase.ui.favorite.util

import com.example.domain.core.Resource
import com.example.domain.remote.Enuygun

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

sealed class FavoriteViewState {
    object Idle : FavoriteViewState()
    object Loading : FavoriteViewState()
    class Error(val error: Resource.Error) : FavoriteViewState()
    class GetAllFavorites(val data: ArrayList<Enuygun>) : FavoriteViewState()
}
