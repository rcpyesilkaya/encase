package com.example.encase.ui.home.util

import android.view.View
import com.example.encase.ui.home.util.HomeViewState

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

data class HomeViewData(val state: HomeViewState) {
    fun progressVisibility() = if (state is HomeViewState.Loading) View.VISIBLE else View.GONE
    fun recyclerViewVisibility() =
        if (state is HomeViewState.FetchList || state is HomeViewState.SortedList || state is HomeViewState.FilterSuccess || state is HomeViewState.FilterErrorEmptyPrice || state is HomeViewState.FilterErrorPriceMinMax) View.VISIBLE else View.GONE

    fun errorVisibility() = if (state is HomeViewState.Error) View.VISIBLE else View.GONE
}
