package com.example.encase.ui.basket.util

import android.view.View

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

data class BasketViewData(val state: BasketViewState) {
    fun progressVisibility() = if (state is BasketViewState.Loading) View.VISIBLE else View.GONE
    fun recyclerViewVisibility() = if (state is BasketViewState.GetAllCartItems) View.VISIBLE else View.GONE
    fun errorVisibility() = if (state is BasketViewState.Error) View.VISIBLE else View.GONE
    fun errorText() = if (state is BasketViewState.Error) state.error.message else "Sepet Bilgisine Ulaşılamadı"
    fun totalPrice() :String{
        var totalPrice=0
        if (state is BasketViewState.GetAllCartItems){
            state.data.forEach {
                totalPrice += it.enuygun.price* it.quantity
            }
        }
       return "$totalPrice TL"
    }
}