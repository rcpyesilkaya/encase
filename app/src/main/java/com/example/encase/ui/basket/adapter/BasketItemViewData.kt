package com.example.encase.ui.basket.adapter

import com.example.domain.local.cartitem.Basket

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

data class BasketItemViewData(
    private val basket: Basket
) {
    fun name() = basket.enuygun.name
    fun priceStr() = basket.enuygun.priceStr
    fun quantity() = basket.quantity.toString()
}
