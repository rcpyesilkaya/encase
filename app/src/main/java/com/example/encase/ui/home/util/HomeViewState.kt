package com.example.encase.ui.home.util

import com.example.domain.core.Resource
import com.example.domain.remote.Enuygun

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

sealed class HomeViewState {
    object Idle : HomeViewState()
    object Loading : HomeViewState()
    class FetchList(val data: ArrayList<Enuygun>) : HomeViewState()
    class Error(val error: Resource.Error) : HomeViewState()
    object FilterErrorEmptyPrice : HomeViewState()
    object FilterErrorPriceMinMax : HomeViewState()
    class FilterSuccess(val data: ArrayList<Enuygun>) : HomeViewState()
    class SortedList(val data: List<Enuygun>) : HomeViewState()
}
