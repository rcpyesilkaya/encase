package com.example.encase

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Recep Yesilkaya on 6.03.2024.
 */

@HiltAndroidApp
class EnUygunApplication : Application()