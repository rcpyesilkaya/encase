package com.example.data.remote.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

data class EnuygunResponse(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("priceStr") val priceStr: String?,
    @SerializedName("price") val price: Int?,
    @SerializedName("desc") val desc: String?,
    @SerializedName("isFavorite") val isFavorite: Boolean?,
)