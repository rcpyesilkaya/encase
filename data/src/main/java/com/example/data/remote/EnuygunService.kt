package com.example.data.remote

import com.example.data.core.GeneralResponse
import com.example.data.remote.response.EnuygunResponse
import retrofit2.http.GET

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

interface EnuygunService {
    @GET("/fetch-list")
    suspend fun fetchList(): GeneralResponse<List<EnuygunResponse>>
}