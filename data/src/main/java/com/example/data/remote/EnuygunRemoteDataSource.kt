package com.example.data.remote

import com.example.data.remote.util.getResult
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

class EnuygunRemoteDataSource @Inject constructor(private val enuygunService: EnuygunService) {

    suspend fun fetchList() = getResult { enuygunService.fetchList() }
}