package com.example.data.core

import com.example.data.core.error.BaseError

sealed class Response<out T> {
    data class Success<out T>(
        val data: T,
        val code: Int,
        val message: String,
        val success: Boolean
    ) : Response<T>()

    data class Error(val exception: BaseError) : Response<Nothing>()
}