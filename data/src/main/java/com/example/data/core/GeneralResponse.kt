package com.example.data.core

import com.google.gson.annotations.SerializedName

data class GeneralResponse<T>(
        @SerializedName("Data") var data: T,
        @SerializedName("Success") var success: Boolean,
        @SerializedName("Message") var message: String? = null,
        @SerializedName("Code") var code: Int? = null,
)