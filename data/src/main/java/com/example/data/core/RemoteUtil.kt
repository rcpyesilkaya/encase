package com.example.data.remote.util

import com.example.data.core.error.BaseError
import com.example.data.core.GeneralResponse
import com.example.data.core.error.RemoteError
import com.example.data.core.RemoteResponse

suspend fun <T> getResult(
    call: suspend () -> GeneralResponse<T>
): RemoteResponse<T> {
    return try {
        val response = call.invoke()
        if (response.success) {
            RemoteResponse.Success(
                data = response.data,
                code = response.code ?: 0,
                message = response.message.orEmpty(),
                success = response.success
            )
        } else {
            RemoteResponse.Error(
                RemoteError.ApiError(
                    response.code,
                    response.message
                )
            )
        }
    } catch (e: Exception) {
        RemoteResponse.Error(BaseError(e.hashCode(), e.message))
    }
}