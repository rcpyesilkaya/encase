package com.example.data.core

import com.example.data.core.error.BaseError

sealed class LocalResponse<out T> : BaseResponse<T> {

    data class Success<out T>(val data: T) : LocalResponse<T>()

    data class Error(val exception: BaseError) : LocalResponse<Nothing>()

    override suspend fun toResponse(): Response<T> {
        return when (this) {
            is Error -> {
                Response.Error(this.exception)
            }
            is Success -> {
                Response.Success(this.data, 200, "", true)
            }
        }
    }
}