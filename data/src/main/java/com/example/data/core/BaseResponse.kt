package com.example.data.core

interface BaseResponse<out T> {
    suspend fun toResponse(): Response<T>
}