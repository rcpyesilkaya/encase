package com.example.data.core.error

open class BaseError(val code: Int?, val message: String?, var throwable: Throwable? = null)