package com.example.data.core

import com.example.data.core.error.BaseError

sealed class RemoteResponse<out T> : BaseResponse<T> {

    data class Success<out T>(val data: T, val code: Int, val message: String, val success: Boolean) : RemoteResponse<T>()

    data class Error(val exception: BaseError) : RemoteResponse<Nothing>()

    override suspend fun toResponse(): Response<T> {
        return when (this) {
            is Error -> {
                Response.Error(this.exception)
            }
            is Success -> {
                Response.Success(this.data, this.code, this.message, this.success)
            }
        }
    }
}