package com.example.data.core.error

sealed class RemoteError(code: Int?, errorMessage: String?, throwable: Throwable? = null) :
    BaseError(code, errorMessage, throwable) {

    class ApiError(code: Int?, message: String?, throwable: Throwable? = null) :
        RemoteError(code, message, throwable)
}