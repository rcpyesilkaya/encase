package com.example.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@Dao
interface EnuygunDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFavorite(entity: EnuygunEntity)

    @Delete
    suspend fun removeFavorite(entity: EnuygunEntity)

    @Query("SELECT * FROM enuygun_table")
    suspend fun getAllFavorites(): List<EnuygunEntity>

    @Query("SELECT * FROM enuygun_table WHERE id = :enuygunId")
    suspend fun getFavoriteItemById(enuygunId: Int): EnuygunEntity?

}