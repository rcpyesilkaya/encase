package com.example.data.local

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@Entity(tableName = "enuygun_table")
@Parcelize
data class EnuygunEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String,
    val priceStr: String,
    val price: Int,
    val desc: String,
    var isFavorite: Boolean
) : Parcelable