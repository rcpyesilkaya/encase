package com.example.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

@Entity(tableName = "cart_items")
data class CartEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val enuygunId: Int,
    var quantity: Int
)
