package com.example.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.data.local.entity.CartEntity

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

@Dao
interface CartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCartItem(cartItem: CartEntity)

    @Update
    suspend fun updateCartItem(cartItem: CartEntity)

    @Delete
    suspend fun deleteCartItem(cartItem: CartEntity)

    @Query("SELECT * FROM cart_items")
    suspend fun getAllCartItems(): List<CartEntity>?

    @Query("SELECT * FROM cart_items WHERE enuygunId = :enuygunId")
    suspend fun getCartItemById(enuygunId: Int): CartEntity?
}