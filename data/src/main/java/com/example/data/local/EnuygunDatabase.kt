package com.example.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.local.entity.CartEntity

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@Database(entities = [EnuygunEntity::class, CartEntity::class], version = 1, exportSchema = false)
abstract class EnuygunDatabase : RoomDatabase() {

    abstract fun enuygunDao(): EnuygunDao
    abstract fun cartDao(): CartDao
}