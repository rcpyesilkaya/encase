package com.example.data.local

import com.example.data.core.LocalResponse
import com.example.data.local.entity.CartEntity
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

class EnuygunLocalDataSource @Inject constructor(
    private val enuygunDao: EnuygunDao,
    private val cartDao: CartDao,
) {

    suspend fun addFavorite(entity: EnuygunEntity): LocalResponse.Success<Unit> {
        return LocalResponse.Success(enuygunDao.addFavorite(entity))
    }

    suspend fun removeFavorite(entity: EnuygunEntity): LocalResponse.Success<Unit> {
        return LocalResponse.Success(enuygunDao.removeFavorite(entity))
    }

    suspend fun getAllFavorites(): LocalResponse.Success<List<EnuygunEntity>> {
        return LocalResponse.Success(enuygunDao.getAllFavorites())
    }

    suspend fun getFavoriteItemById(id: Int): LocalResponse.Success<EnuygunEntity?> {
        return LocalResponse.Success(enuygunDao.getFavoriteItemById(id))
    }

    suspend fun insertCartItem(cartItem: CartEntity): LocalResponse.Success<Unit> {
        return LocalResponse.Success(cartDao.insertCartItem(cartItem))
    }

    suspend fun updateCartItem(cartItem: CartEntity): LocalResponse.Success<Unit> {
        return LocalResponse.Success(cartDao.updateCartItem(cartItem))
    }

    suspend fun deleteCartItem(cartItem: CartEntity): LocalResponse.Success<Unit> {
        return LocalResponse.Success(cartDao.deleteCartItem(cartItem))
    }

    suspend fun getAllCartItems(): LocalResponse.Success<List<CartEntity>?> {
        return LocalResponse.Success(cartDao.getAllCartItems())
    }

    suspend fun getCartItemById(id: Int): LocalResponse.Success<CartEntity?> {
        return LocalResponse.Success(cartDao.getCartItemById(id))
    }

}