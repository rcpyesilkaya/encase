package com.example.data.di

import com.example.data.repository.EnuygunRepository
import com.example.data.repository.EnuygunRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun bindEnuygunRepository(enuygunRepositoryImpl: EnuygunRepositoryImpl): EnuygunRepository

}