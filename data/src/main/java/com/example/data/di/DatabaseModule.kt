package com.example.data.di

import android.content.Context
import androidx.room.Room
import com.example.data.local.CartDao
import com.example.data.local.EnuygunDao
import com.example.data.local.EnuygunDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideEnuygunDatabase(
        @ApplicationContext context: Context
    ): EnuygunDatabase {
        return Room.databaseBuilder(
            context,
            EnuygunDatabase::class.java,
            "enuygun_database"
        ).build()
    }

    @Singleton
    @Provides
    fun provideEnuygunDao(database: EnuygunDatabase): EnuygunDao {
        return database.enuygunDao()
    }

    @Singleton
    @Provides
    fun provideCartDao(database: EnuygunDatabase): CartDao {
        return database.cartDao()
    }
}