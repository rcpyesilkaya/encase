package com.example.data.repository

import com.example.data.core.Response
import com.example.data.local.EnuygunEntity
import com.example.data.local.entity.CartEntity
import com.example.data.remote.response.EnuygunResponse

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

interface EnuygunRepository {
    suspend fun fetchList(): Response<List<EnuygunResponse>>
    suspend fun getFavoriteItemById(id: Int): Response<EnuygunEntity?>
    suspend fun addFavorite(entity: EnuygunEntity): Response<Unit>
    suspend fun removeFavorite(entity: EnuygunEntity): Response<Unit>
    suspend fun getAllFavorites(): Response<List<EnuygunEntity>?>
    suspend fun insertCartItem(cartItem: CartEntity): Response<Unit>
    suspend fun updateCartItem(cartItem: CartEntity): Response<Unit>
    suspend fun deleteCartItem(cartItem: CartEntity): Response<Unit>
    suspend fun getAllCartItems(): Response<List<CartEntity>?>
    suspend fun getCartItemById(enuygunId: Int): Response<CartEntity?>
}