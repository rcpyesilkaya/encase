package com.example.data.repository

import com.example.data.core.Response
import com.example.data.local.EnuygunEntity
import com.example.data.local.EnuygunLocalDataSource
import com.example.data.local.entity.CartEntity
import com.example.data.remote.EnuygunRemoteDataSource
import com.example.data.remote.response.EnuygunResponse
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

class EnuygunRepositoryImpl @Inject constructor(
    private val enuygunRemoteDataSource: EnuygunRemoteDataSource,
    private val enuygunLocalDataSource: EnuygunLocalDataSource,
) : EnuygunRepository {

    override suspend fun fetchList(): Response<List<EnuygunResponse>> {
        return enuygunRemoteDataSource.fetchList().toResponse()
    }

    override suspend fun getFavoriteItemById(id: Int): Response<EnuygunEntity?> {
        return enuygunLocalDataSource.getFavoriteItemById(id).toResponse()
    }

    override suspend fun addFavorite(entity: EnuygunEntity): Response<Unit> {
        return enuygunLocalDataSource.addFavorite(entity).toResponse()
    }

    override suspend fun removeFavorite(entity: EnuygunEntity): Response<Unit> {
        return enuygunLocalDataSource.removeFavorite(entity).toResponse()
    }

    override suspend fun getAllFavorites(): Response<List<EnuygunEntity>?> {
        return enuygunLocalDataSource.getAllFavorites().toResponse()
    }

    override suspend fun insertCartItem(cartItem: CartEntity): Response<Unit> {
        return enuygunLocalDataSource.insertCartItem(cartItem).toResponse()
    }

    override suspend fun updateCartItem(cartItem: CartEntity): Response<Unit> {
        return enuygunLocalDataSource.updateCartItem(cartItem).toResponse()
    }

    override suspend fun deleteCartItem(cartItem: CartEntity): Response<Unit> {
        return enuygunLocalDataSource.deleteCartItem(cartItem).toResponse()
    }

    override suspend fun getAllCartItems(): Response<List<CartEntity>?> {
        return enuygunLocalDataSource.getAllCartItems().toResponse()
    }

    override suspend fun getCartItemById(enuygunId: Int): Response<CartEntity?> {
        return enuygunLocalDataSource.getCartItemById(enuygunId).toResponse()
    }
}