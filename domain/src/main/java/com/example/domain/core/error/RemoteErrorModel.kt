package com.example.domain.core.error

import com.example.domain.core.BaseErrorModel

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

sealed class RemoteErrorModel(code: Int?, errorMessage: String?, throwable: Throwable? = null) :
    BaseErrorModel(code, errorMessage, throwable) {

    class ApiErrorModel(code: Int?, message: String?, throwable: Throwable? = null) :
        RemoteErrorModel(code, message, throwable)
}
