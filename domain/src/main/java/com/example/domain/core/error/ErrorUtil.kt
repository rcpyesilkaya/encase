package com.example.domain.core

import com.example.data.core.error.BaseError
import com.example.data.core.error.RemoteError
import com.example.domain.core.error.ErrorBody
import com.example.domain.core.error.RemoteErrorModel

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */
fun BaseError.mapToDomainError(): ErrorBody {
    return when (this) {
        is RemoteError.ApiError -> {
            ErrorBody(
                RemoteErrorModel.ApiErrorModel(
                    this.code,
                    this.message,
                    this.throwable
                ), showDialog = true
            )
        }
        else -> {
            ErrorBody(
                RemoteErrorModel.ApiErrorModel(
                    this.code,
                    this.message,
                    this.throwable
                ), showDialog = true
            )
        }
    }
}