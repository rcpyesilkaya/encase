package com.example.domain.core

interface Mapper<I, O> {
    fun mapToData(input: I?): O?
}