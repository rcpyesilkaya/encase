package com.example.domain.core

import com.example.domain.core.error.ErrorBody

sealed class Resource<out T> {
    object Loading : Resource<Nothing>()
    class Success<T>(
        val data: T,
        val code: Int? = null,
        val message: String? = null,
        val success: Boolean? = null
    ) : Resource<T>()

    class Error(val code: Int?, val message: String?, val body: ErrorBody? = null) : Resource<Nothing>()
}

inline infix fun <T> Resource<T>.onLoading(action: (Resource.Loading) -> Unit): Resource<T> {
    if (this is Resource.Loading) action(this)
    return this
}

inline infix fun <T> Resource<T>.onSuccess(action: (T) -> Unit): Resource<T> {
    if (this is Resource.Success) action(data)
    return this
}

inline infix fun <T> Resource<T>.onError(action: (Resource.Error) -> Unit): Resource<T> {
    if (this is Resource.Error) action(this)
    return this
}