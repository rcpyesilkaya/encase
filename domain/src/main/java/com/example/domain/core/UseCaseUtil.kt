package com.example.domain.core

import com.example.data.core.Response

suspend fun <T, R> safeUseCaseCall(
    executable: suspend () -> Response<T>,
    mapper: (T) -> R
): Resource<R> {
    return try {
        when (val response = executable.invoke()) {
            is Response.Error -> {
                Resource.Error(
                    response.exception.code,
                    response.exception.message,
                    response.exception.mapToDomainError()
                )
            }
            is Response.Success -> {
                Resource.Success(
                    data = mapper(response.data),
                    code = response.code,
                    message = response.message,
                    success = response.success
                )
            }
        }
    } catch (e: Exception) {
        Resource.Error(400, e.message)
    }
}