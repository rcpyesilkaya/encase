package com.example.domain.core

import com.example.domain.core.error.ErrorType

open class BaseErrorModel(val code: Int?, val message: String?, var throwable: Throwable? = null) : ErrorType