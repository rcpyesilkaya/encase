package com.example.domain.core.error

data class ErrorBody(val type: ErrorType?, val showDialog: Boolean = false)