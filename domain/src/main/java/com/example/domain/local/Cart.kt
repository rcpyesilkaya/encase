package com.example.domain.local

import android.os.Parcelable
import com.example.data.local.entity.CartEntity
import kotlinx.parcelize.Parcelize

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

@Parcelize
data class Cart(
    val enuygunId: Int,
    var quantity: Int
) : Parcelable

fun Cart.toEntity() = CartEntity(
    enuygunId = enuygunId,
    quantity = quantity,
)