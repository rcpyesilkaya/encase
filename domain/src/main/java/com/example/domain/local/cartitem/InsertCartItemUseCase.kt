package com.example.domain.local.cartitem

import com.example.data.IoDispatcher
import com.example.data.core.Response
import com.example.data.repository.EnuygunRepository
import com.example.domain.core.BaseUseCase
import com.example.domain.core.Resource
import com.example.domain.core.safeUseCaseCall
import com.example.domain.local.Cart
import com.example.domain.local.toEntity
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class InsertCartItemUseCase @Inject constructor(
    private val enuygunRepository: EnuygunRepository,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseUseCase<Cart, Unit>(dispatcher) {

    override suspend fun getExecutable(params: Cart): Resource<Unit> {
        return safeUseCaseCall(executable = {
            val existingCartItem = enuygunRepository.getCartItemById(params.enuygunId)
            if (existingCartItem is Response.Success && existingCartItem.data != null) {
                existingCartItem.data!!.quantity += params.quantity
                enuygunRepository.updateCartItem(existingCartItem.data!!)
            } else {
                enuygunRepository.insertCartItem(params.toEntity())
            }
        }, mapper = {
            it
        })
    }
}