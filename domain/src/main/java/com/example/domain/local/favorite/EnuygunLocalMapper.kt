package com.example.domain.local.favorite

import com.example.data.local.EnuygunEntity
import com.example.domain.core.Mapper
import com.example.domain.remote.Enuygun
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

class EnuygunLocalMapper @Inject constructor() : Mapper<EnuygunEntity, Enuygun> {

    override fun mapToData(input: EnuygunEntity?): Enuygun {
        return Enuygun(
            id = input?.id ?: 0,
            name = input?.name ?: "",
            priceStr = input?.priceStr ?: "",
            price = input?.price ?: 0,
            desc = input?.desc ?: "",
            isFavorite = input?.isFavorite ?: false,
        )
    }

    fun mapToDataList(inputList: List<EnuygunEntity>?): List<Enuygun> {
        return inputList?.map {
            mapToData(it)
        } ?: listOf()
    }

    fun mapEntityToEnuygun(input: Enuygun): EnuygunEntity {
        return EnuygunEntity(
            id = input.id,
            name = input.name,
            priceStr = input.priceStr,
            price = input.price,
            desc = input.desc,
            isFavorite = input.isFavorite,
        )
    }
}