package com.example.domain.local.cartitem

import com.example.data.core.Response
import com.example.data.local.entity.CartEntity
import com.example.domain.remote.EnuygunMapper
import com.example.domain.remote.fetchList
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class BasketMapper @Inject constructor(
    private val enuygunMapper: EnuygunMapper,
) {

    fun mapToList(cartEntityList: List<CartEntity>?): ArrayList<Basket> {
        val basketList = arrayListOf<Basket>()
        val enuygunList = enuygunMapper.mapToList((fetchList() as Response.Success).data)

        enuygunList.forEach { enUygun ->
            val cart = cartEntityList?.find { cart -> enUygun.id == cart.enuygunId }
            if (cart != null) {
                basketList.add(
                    Basket(
                        enuygun = enUygun,
                        quantity = cart.quantity ?: 0
                    )
                )
            }
        }
        return basketList
    }

}