package com.example.domain.local.cartitem

import com.example.data.local.entity.CartEntity
import com.example.domain.core.Mapper
import com.example.domain.local.Cart
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class CartEntityMapper @Inject constructor() : Mapper<CartEntity, Cart> {

    override fun mapToData(input: CartEntity?): Cart {
        return Cart(
            enuygunId = input?.enuygunId ?: -1,
            quantity = input?.quantity ?: -1,
        )
    }

    fun mapToDataList(inputList: List<CartEntity>?): List<Cart> {
        return inputList?.map {
            mapToData(it)
        } ?: listOf()
    }
}