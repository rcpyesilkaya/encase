package com.example.domain.local.cartitem

import com.example.data.IoDispatcher
import com.example.data.core.Response
import com.example.data.core.error.BaseError
import com.example.data.repository.EnuygunRepository
import com.example.domain.core.BaseUseCase
import com.example.domain.core.Resource
import com.example.domain.core.safeUseCaseCall
import com.example.domain.local.Cart
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class DeleteCartItemUseCase @Inject constructor(
    private val enuygunRepository: EnuygunRepository,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseUseCase<Cart, Unit>(dispatcher) {

    override suspend fun getExecutable(params: Cart): Resource<Unit> {
        return safeUseCaseCall(executable = {
            val existingCartItem = enuygunRepository.getCartItemById(params.enuygunId)
            if (existingCartItem is Response.Success && existingCartItem.data != null) {
                existingCartItem.data!!.quantity -= params.quantity
                if (existingCartItem.data!!.quantity <= 0 || params.quantity == 0) {
                    enuygunRepository.deleteCartItem(existingCartItem.data!!)
                } else {
                    enuygunRepository.updateCartItem(existingCartItem.data!!)
                }
            } else {
                Response.Error(BaseError(400, "Delete error"))
            }
        }, mapper = {
            it
        })
    }
}