package com.example.domain.local.cartitem

import com.example.data.IoDispatcher
import com.example.data.repository.EnuygunRepository
import com.example.domain.core.BaseUseCase
import com.example.domain.core.Resource
import com.example.domain.core.safeUseCaseCall
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

class GetAllCartItemsUseCase @Inject constructor(
    private val enuygunRepository: EnuygunRepository,
    private val basketMapper: BasketMapper,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseUseCase<Unit, List<Basket>?>(dispatcher) {

    override suspend fun getExecutable(params: Unit): Resource<List<Basket>?> {
        return safeUseCaseCall(executable = {
            enuygunRepository.getAllCartItems()
        }, mapper = {
            basketMapper.mapToList(it)
        })
    }
}