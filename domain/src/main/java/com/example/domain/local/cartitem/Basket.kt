package com.example.domain.local.cartitem

import com.example.domain.remote.Enuygun

/**
 * Created by Recep Yesilkaya on 10.03.2024.
 */

data class Basket(
    val enuygun: Enuygun,
    var quantity: Int
)