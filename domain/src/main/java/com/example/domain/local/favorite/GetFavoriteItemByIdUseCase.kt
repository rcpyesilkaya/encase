package com.example.domain.local.favorite

import com.example.data.IoDispatcher
import com.example.data.repository.EnuygunRepository
import com.example.domain.core.BaseUseCase
import com.example.domain.core.Resource
import com.example.domain.core.safeUseCaseCall
import com.example.domain.local.favorite.EnuygunLocalMapper
import com.example.domain.remote.Enuygun
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 9.03.2024.
 */

class GetFavoriteItemByIdUseCase @Inject constructor(
    private val enuygunRepository: EnuygunRepository,
    private val enuygunLocalMapper: EnuygunLocalMapper,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseUseCase<Int, Enuygun>(dispatcher) {

    override suspend fun getExecutable(params: Int): Resource<Enuygun> {
        return safeUseCaseCall(
            executable = {
                enuygunRepository.getFavoriteItemById(params)
            }, mapper = {
                enuygunLocalMapper.mapToData(it)
            })
    }
}