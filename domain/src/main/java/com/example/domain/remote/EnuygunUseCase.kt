package com.example.domain.remote

import com.example.data.IoDispatcher
import com.example.data.repository.EnuygunRepository
import com.example.domain.core.BaseUseCase
import com.example.domain.core.Resource
import com.example.domain.core.safeUseCaseCall
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

class EnuygunUseCase @Inject constructor(
    private val enuygunRepository: EnuygunRepository,
    private val enuygunMapper: EnuygunMapper,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseUseCase<Unit, List<Enuygun>>(dispatcher) {

    override suspend fun getExecutable(params: Unit): Resource<List<Enuygun>> {
        return safeUseCaseCall(
            executable = {
                //enuygunRepository.fetchList()
                fetchList()
            },
            mapper = {
                enuygunMapper.mapToList(it)
            }
        )
    }
}