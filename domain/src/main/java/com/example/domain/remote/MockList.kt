package com.example.domain.remote

import com.example.data.core.Response
import com.example.data.remote.response.EnuygunResponse

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

fun fetchList(): Response<ArrayList<EnuygunResponse>> {
    val enuygunList = ArrayList<EnuygunResponse>()

     repeat(30){
        enuygunList.add(
            EnuygunResponse(
                id = it+1,
                name = "Mackbook Pro ${it+1}",
                priceStr = " ${(it+1)*1000}TL",
                price = (it+1)*1000,
                desc = "MackBook Pro 2021",
                isFavorite = false
            )
        )
    }

    return Response.Success(enuygunList, code = 200, "", true)
}