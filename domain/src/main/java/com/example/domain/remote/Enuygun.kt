package com.example.domain.remote

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

@Parcelize
data class Enuygun(
    val id: Int,
    val name: String,
    val priceStr: String,
    val price: Int,
    val desc: String,
    var isFavorite: Boolean,
) : Parcelable
