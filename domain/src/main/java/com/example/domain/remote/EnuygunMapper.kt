package com.example.domain.remote

import com.example.data.remote.response.EnuygunResponse
import com.example.domain.core.Mapper
import javax.inject.Inject

/**
 * Created by Recep Yesilkaya on 8.03.2024.
 */

class EnuygunMapper @Inject constructor() : Mapper<EnuygunResponse, Enuygun> {

    override fun mapToData(input: EnuygunResponse?): Enuygun {
        return Enuygun(
            id = input?.id ?: 0,
            name = input?.name ?: "",
            priceStr = input?.priceStr ?: "",
            price = input?.price ?: 0,
            desc = input?.desc ?: "",
            isFavorite = input?.isFavorite ?: false,
        )
    }

    fun mapToList(inputList: List<EnuygunResponse>?): List<Enuygun> {
        return inputList?.map {
            mapToData(it)
        } ?: listOf()
    }
}