plugins {
    id(Plugins.androidLibrary)
    id(Plugins.jetbrainsAndroidLibrary)
    id(Plugins.kotlinKapt)
    id(Plugins.daggerHiltAndroid)
    id(Plugins.kotlinParcelize)
}

android {
    namespace = "com.example.domain"
    setCompileSdkVersion(com.example.builsSrc.Config.compileSdk)

    defaultConfig {
        minSdk = com.example.builsSrc.Config.minSdk
        targetSdk = com.example.builsSrc.Config.targetSdk

        testInstrumentationRunner = com.example.builsSrc.Config.testInstrumentationRunner
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(project(mapOf("path" to Modules.data)))
    implementation(Dependencies.hiltAndroid)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.appcompat)
    implementation(Dependencies.material)
    implementation(Dependencies.coroutinesAndroid)
    implementation(Dependencies.coroutinesCore)
    implementation(Dependencies.room)
    implementation(Dependencies.roomKtx)

    kapt(Dependencies.hiltCompiler)
    kapt(Dependencies.hiltAndroidCompiler)
    kapt(Dependencies.roomComplier)

    testImplementation(Dependencies.junit)
    testImplementation(Dependencies.coreTesting)
    testImplementation(Dependencies.kotlinxCoroutinesTest)
    testImplementation(Dependencies.truth)
    testImplementation(Dependencies.mockitoCore)
    testImplementation(Dependencies.mockitoInline)
    testImplementation(Dependencies.extJunit)
    testImplementation(Dependencies.robolectric)

    androidTestImplementation(Dependencies.junit)
    androidTestImplementation(Dependencies.mockitoAndroid)
    androidTestImplementation(Dependencies.kotlinxCoroutinesTest)
    androidTestImplementation(Dependencies.coreTesting)
    androidTestImplementation(Dependencies.truth)
    androidTestImplementation(Dependencies.mockitoCore)
    androidTestImplementation(Dependencies.mockitoInline)
    androidTestImplementation(Dependencies.extJunit)
    androidTestImplementation(Dependencies.espressoCore)
}