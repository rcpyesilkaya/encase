/**
 * Created by Recep Yesilkaya on 6.03.2024.
 */
package com.example.builsSrc

object ClassPaths {
    val hiltAndroidGradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:${Versions.daggerHiltVersion}"
    val gradle = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinGradleVersion}"
    val navigationSafeArgs = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigationVersion}"

}