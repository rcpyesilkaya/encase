/**
 * Created by Recep Yesilkaya on 6.03.2024.
 */

object Dependencies {
    val coreKtx = "androidx.core:core-ktx:${Versions.coreKtxVersion}"
    val appcompat = "androidx.appcompat:appcompat:${Versions.appcompatVersion}"
    val material = "com.google.android.material:material:${Versions.materialVersion}"
    val constraintlayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintlayoutVersion}"
    val livedataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.livedataKtxVersion}"
    val viewmodelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.viewmodelKtxVersion}"
    val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigationVersion}"
    val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigationVersion}"
    val junit = "junit:junit:${Versions.junitVersion}"
    val extJunit = "androidx.test.ext:junit:${Versions.extJunitVersion}"
    val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCoreVersion}"
    val hiltAndroid = "com.google.dagger:hilt-android:${Versions.daggerHiltVersion}"
    val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    val hiltAndroidCompiler =
        "com.google.dagger:hilt-android-compiler:${Versions.daggerHiltVersion}"
    val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltCompilerVersion}"
    val kotlinxCoroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.kotlinxCoroutinesTestVersion}"
    val truth = "com.google.truth:truth:${Versions.truthVersion}"
    val mockitoCore = "org.mockito:mockito-core:${Versions.mockitoCoreVersion}"
    val mockitoInline = "org.mockito:mockito-inline:${Versions.mockitoInlineVersion}"
    val robolectric = "org.robolectric:robolectric:${Versions.robolectricVersion}"
    val mockitoAndroid = "org.mockito:mockito-android:${Versions.mockitoAndroidVersion}"
    val coreTesting = "androidx.arch.core:core-testing:${Versions.coreTestingVersion}"
    val coroutinesAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutinesVersion}"
    val coroutinesCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutinesVersion}"
    val converterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
    val loggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.loggingInterceptorVersion}"
    val room = "androidx.room:room-runtime:${Versions.roomVersion}"
    val roomKtx = "androidx.room:room-ktx:${Versions.roomVersion}"
    val roomComplier = "androidx.room:room-compiler:${Versions.roomVersion}"
}