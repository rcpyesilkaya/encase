/**
 * Created by Recep Yesilkaya on 6.03.2024.
 */

object Plugins {
    const val androidApplication = "com.android.application"
    const val jetbrainsAndroidLibrary = "org.jetbrains.kotlin.android"
    const val androidLibrary = "com.android.library"
    const val jetBrainsKotlinJvm = "org.jetbrains.kotlin.jvm"
    const val kotlinKapt = "kotlin-kapt"
    const val daggerHiltAndroid = "dagger.hilt.android.plugin"
    const val kotlinParcelize = "kotlin-parcelize"
    const val navigationSafeArgs = "androidx.navigation.safeargs.kotlin"

}