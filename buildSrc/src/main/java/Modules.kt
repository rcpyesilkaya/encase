/**
 * Created by Recep Yesilkaya on 6.03.2024.
 */

object Modules {
    val data = ":data"
    val domain = ":domain"
    val app = ":app"
}