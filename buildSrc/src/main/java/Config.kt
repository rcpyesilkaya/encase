/**
 * Created by Recep Yesilkaya on 6.03.2024.
 */

package com.example.builsSrc

object Config {
    val compileSdk = 34
    val minSdk = 24
    val targetSdk = 34
    val versionCode = 1
    val versionName = "1.0"
    val applicationId = "com.example.encase"
    val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}